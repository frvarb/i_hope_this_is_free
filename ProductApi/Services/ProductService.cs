using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ProductApi.Data;
using ProductApi.Dtos;
using ProductApi.Models;
using ProductApi.Validators;

namespace ProductApi.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductStoreRepo _productStoreRepo;

        public ProductService(IProductStoreRepo productStoreRepo)
        {
            _productStoreRepo = productStoreRepo;
        }
        // Product must have ProductStore loaded (not lazy load)
        public void attachStoreToProduct([FromQuery] Product product, [FromBody] ProductUpdateDto productCreateDto) {
            if (product.ProductStores != null) 
            {
                foreach (var productStore in product.ProductStores.ToList())
                {
                    // Remove the ProductStore that is not in storeIds list
                    if (!productCreateDto.Stores.Contains(productStore.StoreId))
                    {
                        product.ProductStores.Remove(productStore);
                    }
                }
            }

            foreach (var newStoreId in productCreateDto.Stores)
            {
                // Add the ProductStores that are not in the list of Product Stores
                if (product.ProductStores == null || !product.ProductStores.Any(s => s.StoreId == newStoreId))
                {
                    var newProductStore = new ProductStore { StoreId = newStoreId, ProductId = product.Id };
                    _productStoreRepo.AddProductStore(newProductStore);
                }
            }
            _productStoreRepo.SaveChanges();
        }

        // Assuming VAT is in percentages
        public void SetMissingFields(Product productModel) {
            if (productModel.Price == null) {
                productModel.Price = productModel.PriceWithVAT / (productModel.VAT/100 + 1);
            } else if (productModel.VAT == null) {
                productModel.VAT = 100 * ((productModel.PriceWithVAT / productModel.Price) - 1);
            } else if (productModel.PriceWithVAT == null) {
                productModel.PriceWithVAT = productModel.Price * (productModel.VAT/100 + 1);
            }
        }
    }
}