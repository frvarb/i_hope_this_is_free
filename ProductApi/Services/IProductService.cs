using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ProductApi.Dtos;
using ProductApi.Models;

namespace ProductApi.Services
{
    public interface IProductService
    {
         public void attachStoreToProduct([FromQuery] Product product, [FromBody] ProductUpdateDto productCreateDto);

         public void SetMissingFields(Product productModel);

    }
}