using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ProductApi.Models
{
    [Index(nameof(Product.Name), IsUnique = true)]
    public class Product
    {

        [Key]
        public long Id { get;set; }
        [Required]
        [MaxLength(250)]
        public string Name { get;set; }
        [Required]
        public long GroupId { get; set; }
        public Group Group { get; set; }
        public List<ProductStore> ProductStores { get;set; }
        [Required]
        public DateTime Inserted { get;set; }
        public decimal? Price { get;set; }
        public decimal? PriceWithVAT { get;set; }
        public decimal? VAT { get;set; }

    }
}