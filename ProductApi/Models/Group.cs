using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace ProductApi.Models
{
    [Index(nameof(Group.Name), IsUnique = true)]
    public class Group
    {
        [Key]
        public long Id { get;set; }
        public long? ParentId { get;set; }
        public Group Parent { get;set; }
        [Required]
        [MaxLength(250)]
        public string Name { get;set; }
        public List<Group> SubGroups { get;set; }
    }
}