using System.ComponentModel.DataAnnotations;

namespace ProductApi.Models
{
    public class ProductStore
    {            
        [Required]
        public long ProductId { get; set; }
        public Product Product { get; set; }
        [Required]
        public long StoreId { get; set; }
        public Store Store { get; set; }
    }
}