using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace ProductApi.Models
{
    [Index(nameof(Store.Name), IsUnique = true)]
    public class Store
    {
        [Key]
        public long Id { get; set; }
        [Required]
        [MaxLength(250)]
        public string Name { get; set; }
        public List<ProductStore> ProductStores { get; set; }
    }
}