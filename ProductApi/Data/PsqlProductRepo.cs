using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProductApi.Models;

namespace ProductApi.Data
{
    public class PsqlProductRepo : IProductRepo
    {
        private readonly ApiContext _context;
        public PsqlProductRepo(ApiContext context)
        {
            _context = context;
        }
        public void CreateProduct(Product product)
        {
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            _context.Product.Add(product);
        }

        public void DeleteProduct(Product product)
        {
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            _context.Product.Remove(product);
        }

        public List<Product> GetAllProducts()
        {
            return _context.Product.Include(p => p.Group)
            .Include(p => p.ProductStores).ThenInclude(p => p.Store).ToList();
        }

        public Product GetProductById(long id)
        {
            return _context.Product.Include(p => p.Group)
            .Include(p => p.ProductStores).ThenInclude(p => p.Store).FirstOrDefault(p => p.Id == id);
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void UpdateProduct(Product product)
        {
            // Not implemented
        }
    }
}