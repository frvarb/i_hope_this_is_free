using System.Collections.Generic;
using ProductApi.Models;

namespace ProductApi.Data
{
    public interface IStoreRepo
    {
        bool SaveChanges();
         List<Store> GetAllStores();
         Store GetStoreById(long id);
         void CreateStore(Store store);
         void UpdateStore(Store store);
         void DeleteStore(Store store);
    }
}