using System;
using ProductApi.Models;

namespace ProductApi.Data
{
    // Didnt make an interface just for one function
    public class PsqlProductStoreRepo : IProductStoreRepo
    {
        private readonly ApiContext _context;
        public PsqlProductStoreRepo(ApiContext context)
        {
            _context = context;
        }

        public void AddProductStore(ProductStore productStore)
        {
            if (productStore == null)
            {
                throw new ArgumentNullException(nameof(productStore));
            }
            _context.ProductStore.Add(productStore);
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }
    }
}