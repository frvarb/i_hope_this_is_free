using System.Collections.Generic;
using ProductApi.Models;

namespace ProductApi.Data
{
    public interface IGroupRepo
    {
        bool SaveChanges();
         List<Group> GetAllGroups();
         Group GetGroupById(long id);
         void CreateGroup(Group group);
         void UpdateGroup(Group group);
         void DeleteGroup(Group group);
         Group GetGroupByIdAndSubGroups(long id);
    }
}