
using Microsoft.EntityFrameworkCore;
using ProductApi.Models;

namespace ProductApi.Data
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) : base(options)
        {
            
        }

        public DbSet<Store> Store { get; set; }

        public DbSet<Product> Product { get; set; }

        public DbSet<Group> Group { get; set; }

        public DbSet<ProductStore> ProductStore { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //ProductStore
            //No duplicate product relations to store
            modelBuilder.Entity<ProductStore>()
                .HasKey(bc => new { bc.ProductId, bc.StoreId });  

            modelBuilder.Entity<ProductStore>()
                .HasOne(bc => bc.Product)
                .WithMany(b => b.ProductStores)
                .HasForeignKey(bc => bc.ProductId);  

            modelBuilder.Entity<ProductStore>()
                .HasOne(bc => bc.Store)
                .WithMany(c => c.ProductStores)
                .HasForeignKey(bc => bc.StoreId);
            //Store
            modelBuilder.Entity<Store>()
                .HasMany(c => c.ProductStores)
                .WithOne(e => e.Store);
            //Group
            modelBuilder.Entity<Group>()
                .HasOne(x=> x.Parent)
                .WithMany(x=> x.SubGroups)
                .HasForeignKey(x=> x.ParentId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Restrict);
            //Product
            modelBuilder.Entity<Product>()
                .HasMany(c => c.ProductStores)
                .WithOne(e => e.Product);
            modelBuilder.Entity<Product>()
                .Property(b => b.Inserted)
                .HasDefaultValueSql("now()");
        }
    }
}