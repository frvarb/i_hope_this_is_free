using ProductApi.Models;

namespace ProductApi.Data
{
    public interface IProductStoreRepo
    {
         void AddProductStore(ProductStore productStore);
         bool SaveChanges();
    }
}