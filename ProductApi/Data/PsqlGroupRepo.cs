using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProductApi.Models;

namespace ProductApi.Data
{
    public class PsqlGroupRepo : IGroupRepo
    {
        private readonly ApiContext _context;
        public PsqlGroupRepo(ApiContext context)
        {
            _context = context;
        }
        public void CreateGroup(Group group)
        {
            if (group == null)
            {
                throw new ArgumentNullException(nameof(group));
            }
            _context.Group.Add(group);
        }

        public void DeleteGroup(Group group)
        {
            if (group == null)
            {
                throw new ArgumentNullException(nameof(group));
            }
            _context.Group.Remove(group);
        }

        public List<Group> GetAllGroups()
        {
            return _context.Group.ToList();
        }

        public Group GetGroupById(long id)
        {
            // doesnt get nested elementes
            return _context.Group.FirstOrDefault(p => p.Id == id);
        }

        public Group GetGroupByIdAndSubGroups(long id)
        {
            // toList gets nested elements aswell
            return _context.Group.ToList().FirstOrDefault(p => p.Id == id);
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void UpdateGroup(Group group)
        {
            // Not implemented
        }
    }
}