using System.Collections.Generic;
using ProductApi.Models;

namespace ProductApi.Data
{
    public interface IProductRepo
    {
        bool SaveChanges();
         List<Product> GetAllProducts();
         Product GetProductById(long id);
         void CreateProduct(Product product);
         void UpdateProduct(Product product);
         void DeleteProduct(Product product);
    }
}