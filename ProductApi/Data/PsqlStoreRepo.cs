using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProductApi.Models;

namespace ProductApi.Data
{
    public class PsqlStoreRepo : IStoreRepo
    {

        private readonly ApiContext _context;
        public PsqlStoreRepo(ApiContext context)
        {
            _context = context;
        }

        public void CreateStore(Store store)
        {
            if (store == null)
            {
                throw new ArgumentNullException(nameof(store));
            }
            _context.Store.Add(store);
        }

        public void DeleteStore(Store store)
        {
            if (store == null)
            {
                throw new ArgumentNullException(nameof(store));
            }
            _context.Store.Remove(store);
        }

        public List<Store> GetAllStores()
        {
            return _context.Store.Include(p => p.ProductStores).ThenInclude(p => p.Product).ToList();
            //return _context.Store.ToList();
        }

        public Store GetStoreById(long id)
        {
            return _context.Store.Include(p => p.ProductStores).ThenInclude(p => p.Product).FirstOrDefault(p => p.Id == id);
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void UpdateStore(Store store)
        {
            // Not implemented
        }
    }
}