using System.Collections.Generic;
using ProductApi.Models;

namespace ProductApi.Dtos
{
    public class StoreReadDto
    {
        public long Id { get;set; }
        public string Name { get;set; }
        public List<ProductNameAndIdDto> ProductStores { get;set; }
    }
}