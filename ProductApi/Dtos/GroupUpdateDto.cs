using System.ComponentModel.DataAnnotations;

namespace ProductApi.Dtos
{
    public class GroupUpdateDto
    {
        public long? ParentId { get;set; }
        [Required]
        [MaxLength(250)]
        public string Name { get;set; }
    }
}