namespace ProductApi.Dtos
{
    public class ProductNameAndIdDto
    {
        public long Id { get;set; }
        public string Name { get;set; }
    }
}