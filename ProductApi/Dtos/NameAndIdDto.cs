namespace ProductApi.Dtos
{
    public class NameAndIdDto
    {
        public long Id { get;set; }
        public string Name { get;set; }
    }
}