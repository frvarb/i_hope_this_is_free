using System.Collections.Generic;

namespace ProductApi.Dtos
{
    public class GroupReadDto
    {
        public long Id { get;set; }
        public long? ParentId { get;set; }
        public string Name { get;set; }
        public List<GroupReadDto> SubGroups { get;set; }
    }
}