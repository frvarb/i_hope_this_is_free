using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProductApi.Dtos
{
    public class ProductUpdateDto
    {
        [Required]
        public long? GroupId { get;set; }
        [Required]
        [MaxLength(250)]
        public string Name { get;set; }
        public List<long> Stores { get;set; }
        public decimal? Price { get;set; }
        public decimal? PriceWithVAT { get;set; }
        public decimal? VAT { get;set; }
    }
}