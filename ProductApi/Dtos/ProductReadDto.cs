using System;
using System.Collections.Generic;

namespace ProductApi.Dtos
{
    public class ProductReadDto
    {
        private DateTime _inserted;
        public long Id { get;set; }
        public NameAndIdDto Group { get;set; }
        public string Name { get;set; }
        public List<StoreNameAndIdDto> ProductStores { get;set; }
        public DateTime Inserted 
        { 
            // DB does not hold local time, so it needs to be converted
            get { return _inserted.ToLocalTime(); } 
            set { _inserted = value; } 
        }
        public decimal Price { get;set; }
        public decimal PriceWithVAT { get;set; }
        public decimal VAT { get;set; }
    }
}