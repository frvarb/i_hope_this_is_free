using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProductApi.Dtos
{
    public class StoreUpdateDto
    {
        [Required]
        [MaxLength(250)]
        public string Name { get;set; }

        public List<long> ProductIds;
    }
}