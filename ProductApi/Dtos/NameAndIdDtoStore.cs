namespace ProductApi.Dtos
{
    public class StoreNameAndIdDto
    {
        public long Id { get;set; }
        public string Name { get;set; }
    }
}