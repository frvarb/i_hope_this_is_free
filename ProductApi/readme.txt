dotnet add package Microsoft.EntityFrameworkCore
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet tool install --global dotnet-ef
dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL
dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL.Design
dotnet add package Microsoft.AspNetCore.Mvc

// For mapping
dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection

// For validation
dotnet add package FluentValidation
dotnet add package FluentValidation.AspNetCore

// For patch requests
dotnet add package Microsoft.AspNetCore.JsonPatch
dotnet add package Microsoft.AspNetCore.Mvc.NewtonsoftJson

// To create database tables
dotnet migrations add InitialMigration 
dotnet ef database update