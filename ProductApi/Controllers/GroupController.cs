using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using ProductApi.Data;
using ProductApi.Dtos;
using ProductApi.Models;

namespace ProductApi.Controllers
{
    [Route("/groups")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        private readonly IGroupRepo _repo;
        private readonly IMapper _mapper;
        public GroupController(IGroupRepo repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult <List<GroupReadDto>> GetAllGroups() 
        {
            var groupItems = _repo.GetAllGroups();
            return Ok(_mapper.Map<List<GroupReadDto>>(groupItems));
        }

        [HttpGet("{id}", Name="GetGroupById")]
        public ActionResult <GroupReadDto> GetGroupById(long id) 
        {
            var groupItem = _repo.GetGroupByIdAndSubGroups(id);
            if (groupItem != null) {
                return Ok(_mapper.Map<GroupReadDto>(groupItem));
            }
            return NotFound();
        }

        [HttpPost]
        public ActionResult <GroupReadDto> CreateGroup(GroupUpdateDto groupCreateDto) 
        {
            var groupModel = _mapper.Map<Group>(groupCreateDto);
            _repo.CreateGroup(groupModel);
            _repo.SaveChanges();
            var groupReadDto = _mapper.Map<GroupReadDto>(groupModel);
            return CreatedAtRoute(nameof(GetGroupById), new {Id = groupReadDto.Id}, groupReadDto); 
        }

        
        [HttpPut("{id}")]
        public ActionResult <GroupReadDto> UpdateGroup(long id, GroupUpdateDto groupCreateDto) 
        {
            var groupItem = _repo.GetGroupById(id);
            if (groupItem == null) 
            {
                return NotFound();
            }
            _mapper.Map(groupCreateDto, groupItem);
            _repo.UpdateGroup(groupItem);
            _repo.SaveChanges();
            return Ok(_mapper.Map<GroupReadDto>(groupItem)); 
        }

        // Patch update
        // Sample accepted request body:
        // This request body will replace "name" attribute
        // {
        //      "op": "replace",
        //      "path": "/name",
        //      "value": "Krossi"
        // }
        [HttpPatch("{id}")]
        public ActionResult <GroupReadDto> PartialGroupUpdate(long id, JsonPatchDocument<GroupUpdateDto> patchDoc)
        {
            // get item from base
            var groupItem = _repo.GetGroupById(id);
            if (groupItem == null) 
            {
                return NotFound();
            }
            // map stored item to updateDto to apply changes to the dto
            var groupUpdateDto = _mapper.Map<GroupUpdateDto>(groupItem);
            // apply changes to the  updateDto
            patchDoc.ApplyTo(groupUpdateDto, ModelState);
            // validate updateDto
            if (!TryValidateModel(groupUpdateDto)) 
            {
                return ValidationProblem(ModelState);
            }
            // put applied changes to the base
            _mapper.Map(groupUpdateDto, groupItem);
            _repo.UpdateGroup(groupItem);
            _repo.SaveChanges();
            return Ok(_mapper.Map<GroupReadDto>(groupItem));
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteGroup(long id)
        {
            // get item from base
            var groupItem = _repo.GetGroupByIdAndSubGroups(id);
            if (groupItem == null) 
            {
                return NotFound();
            }
            // remove references to parent from children
            foreach (var group in groupItem.SubGroups)
            {
                group.Parent = null;
            }
            _repo.DeleteGroup(groupItem);
            _repo.SaveChanges();
            return Ok();    
        }
    }
}