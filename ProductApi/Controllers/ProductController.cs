using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ProductApi.Data;
using ProductApi.Dtos;
using ProductApi.Models;
using ProductApi.Services;
using ProductApi.Validators;

namespace ProductApi.Controllers
{
    [Route("/products")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepo _repo;

        private readonly IProductStoreRepo _productStoreRepo;
        private readonly IMapper _mapper;

        private readonly IProductService _productService;
        public ProductController(IProductRepo repo, IProductStoreRepo productStoreRepo, IMapper mapper, IProductService productService)
        {
            _repo = repo;
            _mapper = mapper;
            _productStoreRepo = productStoreRepo;
            _productService = productService;
        }
        [HttpGet]
        public ActionResult <List<ProductReadDto>> GetAllProducts() 
        {
            var products = _repo.GetAllProducts();
            return Ok(_mapper.Map<List<ProductReadDto>>(products));
        }

        [HttpGet("{id}", Name="GetProductById")]
        public ActionResult <ProductReadDto> GetProductById(long id) 
        {
            // prolly need to change
            var product = _repo.GetProductById(id);
            if (product != null) 
            {
                return Ok(_mapper.Map<ProductReadDto>(product));
            }
            return NotFound();
        }

        [HttpPost]
        public ActionResult <ProductReadDto> CreateProduct(ProductUpdateDto productCreateDto, 
            [FromServices] IOptions<ApiBehaviorOptions> apiBehaviorOptions) 
        {
            var productModel = _mapper.Map<Product>(productCreateDto);
            // Validate VAT, Price and PriceWithVAT
            var actionResult = ValidateProductModel(productModel, apiBehaviorOptions);
            if (actionResult != null) {
                return actionResult;
            }
            _repo.CreateProduct(productModel);
            _repo.SaveChanges();
            // product model has id now
            _productService.attachStoreToProduct(productModel, productCreateDto);
            // get product from base again with attached stores
            var productReadDto = _mapper.Map<ProductReadDto>(_repo.GetProductById(productModel.Id));
            // returns code 201 (created)
            return CreatedAtRoute(nameof(GetProductById), new {Id = productReadDto.Id}, productReadDto); 
        }

        
        [HttpPut("{id}")]
        public ActionResult <ProductReadDto> UpdateProduct(long id, ProductUpdateDto productCreateDto, 
        [FromServices] IOptions<ApiBehaviorOptions> apiBehaviorOptions) 
        {
            var productModel = _repo.GetProductById(id);
            if (productModel == null) 
            {
                return NotFound();
            }
            _mapper.Map(productCreateDto, productModel);
            _productService.attachStoreToProduct(productModel, productCreateDto);
            // Validate VAT, Price and PriceWithVAT
            var actionResult = ValidateProductModel(productModel, apiBehaviorOptions);
            if (actionResult != null) {
                return actionResult;
            }
            _repo.UpdateProduct(productModel);
            _repo.SaveChanges();
            // GetProductById hooks group to the updated product dto
            return Ok(_mapper.Map<ProductReadDto>(_repo.GetProductById(productModel.Id))); 
        }

        // Patch update
        // Sample accepted request body:
        // This request body will replace "name" attribute
        // {
        //      "op": "replace",
        //      "path": "/name",
        //      "value": "Krossi"
        // }
        [HttpPatch("{id}")]
        public ActionResult <ProductReadDto> PartialProductUpdate(long id, JsonPatchDocument<ProductUpdateDto> patchDoc)
        {
            // get item from base
            var productModel = _repo.GetProductById(id);
            if (productModel == null) 
            {
                return NotFound();
            }
            // map stored item to updateDto to apply changes to the dto
            var productUpdateDto = _mapper.Map<ProductUpdateDto>(productModel);
            // apply changes to the  updateDto
            patchDoc.ApplyTo(productUpdateDto, ModelState);
            // validate updateDto
            if (!TryValidateModel(productUpdateDto)) 
            {
                return ValidationProblem(ModelState);
            }
            // put applied changes to the base
            _mapper.Map(productUpdateDto, productModel);
            _repo.UpdateProduct(productModel);
            _repo.SaveChanges();
            return Ok(_mapper.Map<ProductReadDto>(productModel));
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteProduct(long id)
        {
            // get item from base
            var productModel = _repo.GetProductById(id);
            if (productModel == null) 
            {
                return NotFound();
            }
            // remove references from store to product
            productModel.ProductStores.Clear();
            _repo.DeleteProduct(productModel);
            _repo.SaveChanges();
            return Ok();    
        }

        private ActionResult ValidateProductModel(Product productModel, [FromServices] IOptions<ApiBehaviorOptions> apiBehaviorOptions) {
            ProductValidator validator = new ProductValidator();
            ValidationResult result = validator.Validate(productModel);
            if(!result.IsValid) 
            {
                // same format of exception as other fields
                ModelState.AddModelError(result.Errors[0].PropertyName, result.Errors[0].ErrorMessage);
                return (ActionResult)apiBehaviorOptions.Value.InvalidModelStateResponseFactory(ControllerContext);
            }
            _productService.SetMissingFields(productModel);
            return null;
        }
    }
}