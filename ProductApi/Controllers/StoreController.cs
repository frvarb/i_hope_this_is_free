using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using ProductApi.Data;
using ProductApi.Dtos;
using ProductApi.Models;

namespace ProductApi.Controllers
{
    [Route("/stores")]
    [ApiController]
    public class StoreController : ControllerBase
    {
        private readonly IStoreRepo _repo;
        private readonly IMapper _mapper;
        public StoreController(IStoreRepo repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult <List<StoreReadDto>> GetAllStores() 
        {
            var storeModels = _repo.GetAllStores();
            return Ok(_mapper.Map<List<StoreReadDto>>(storeModels));
        }

        [HttpGet("{id}", Name="GetStoreById")]
        public ActionResult <StoreReadDto> GetStoreById(long id) 
        {
            var storeModel = _repo.GetStoreById(id);
            if (storeModel != null) {
                return Ok(_mapper.Map<StoreReadDto>(storeModel));
            }
            return NotFound();
        }

        [HttpPost]
        public ActionResult <StoreReadDto> CreateStore(StoreUpdateDto storeCreateDto) 
        {
            var storeModel = _mapper.Map<Store>(storeCreateDto);
            _repo.CreateStore(storeModel);
            _repo.SaveChanges();
            var storeReadDto = _mapper.Map<StoreReadDto>(storeModel);
            return CreatedAtRoute(nameof(GetStoreById), new {Id = storeReadDto.Id}, storeReadDto); 
        }

        
        [HttpPut("{id}")]
        public ActionResult <StoreReadDto> UpdateStore(long id, StoreUpdateDto storeCreateDto) 
        {
            var storeModel = _repo.GetStoreById(id);
            if (storeModel == null) 
            {
                return NotFound();
            }
            _mapper.Map(storeCreateDto, storeModel);
            _repo.UpdateStore(storeModel);
            _repo.SaveChanges();
            return Ok(_mapper.Map<StoreReadDto>(storeModel)); 
        }

        // Patch update
        // Sample accepted request body:
        // This request body will replace "name" attribute
        // {
        //      "op": "replace",
        //      "path": "/name",
        //      "value": "Krossi"
        // }
        [HttpPatch("{id}")]
        public ActionResult <StoreReadDto> PartialStoreUpdate(long id, JsonPatchDocument<StoreUpdateDto> patchDoc)
        {
            // get item from base
            var storeModel = _repo.GetStoreById(id);
            if (storeModel == null) 
            {
                return NotFound();
            }
            // map stored item to updateDto to apply changes to the dto
            var storeUpdateDto = _mapper.Map<StoreUpdateDto>(storeModel);
            // apply changes to the  updateDto
            patchDoc.ApplyTo(storeUpdateDto, ModelState);
            // validate updateDto
            if (!TryValidateModel(storeUpdateDto)) 
            {
                return ValidationProblem(ModelState);
            }
            // put applied changes to the base
            _mapper.Map(storeUpdateDto, storeModel);
            _repo.UpdateStore(storeModel);
            _repo.SaveChanges();
            return Ok(_mapper.Map<StoreReadDto>(storeModel));
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteStore(long id)
        {
            // get item from base
            var storeModel = _repo.GetStoreById(id);
            if (storeModel == null) 
            {
                return NotFound();
            }
            // remove references from store to product
            storeModel.ProductStores.Clear();
            _repo.DeleteStore(storeModel);
            _repo.SaveChanges();
            return Ok();    
        }
    }
}