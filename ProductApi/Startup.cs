using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using ProductApi.Data;
using Newtonsoft.Json.Serialization;
using ProductApi.Services;

namespace ProductApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Set up psql db connection
            services.AddDbContext<ApiContext>(options => options.UseNpgsql(Configuration.GetConnectionString("StoreConnection")));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);
            // Add controllers and Patch Requests
            services.AddControllers().AddNewtonsoftJson(options => {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                // cut off loops, Group children might become looped for example
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });
            // Add automapper
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            // Link interface to repo
            services.AddScoped<IStoreRepo, PsqlStoreRepo>();
            services.AddScoped<IGroupRepo, PsqlGroupRepo>();
            services.AddScoped<IProductRepo, PsqlProductRepo>();
            services.AddScoped<IProductStoreRepo, PsqlProductStoreRepo>();
            // Link interface to service
            services.AddScoped<IProductService, ProductService>();
            
            services.AddSwaggerGen();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
            });
        }
    }
}
