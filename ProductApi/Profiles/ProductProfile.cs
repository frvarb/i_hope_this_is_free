using AutoMapper;
using ProductApi.Dtos;
using ProductApi.Models;

namespace ProductApi.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            // <Source, Target>
            CreateMap<Product, ProductReadDto>();
            CreateMap<ProductUpdateDto, Product>();
            CreateMap<Product, ProductUpdateDto>();
        }
    }
}