using AutoMapper;
using ProductApi.Dtos;
using ProductApi.Models;

namespace ProductApi.Profiles
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
            // <Source, Target>
            CreateMap<Group, GroupReadDto>();
            CreateMap<GroupUpdateDto, Group>();
            CreateMap<Group, GroupUpdateDto>();
            CreateMap<Group, NameAndIdDto>();
        }
    }
}