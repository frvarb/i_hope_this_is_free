using AutoMapper;
using ProductApi.Dtos;
using ProductApi.Models;

namespace ProductApi.Profiles
{
    public class StoreProfile : Profile
    {
        public StoreProfile()
        {
            // <Source, Target>
            CreateMap<Store, StoreReadDto>();
            CreateMap<StoreUpdateDto, Store>();
            CreateMap<Store, StoreUpdateDto>();
            CreateMap<ProductStore, StoreNameAndIdDto>()
                .ForMember(p => p.Name, p => p.MapFrom(src => src.Store.Name))
                .ForMember(p => p.Id, p => p.MapFrom(src => src.Store.Id));
            CreateMap<ProductStore, ProductNameAndIdDto>()
                .ForMember(p => p.Name, p => p.MapFrom(src => src.Product.Name))
                .ForMember(p => p.Id, p => p.MapFrom(src => src.Product.Id));
        }
    }
}