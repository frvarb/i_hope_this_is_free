using FluentValidation;
using ProductApi.Models;

namespace ProductApi.Validators
{
    public class ProductValidator:AbstractValidator<Product>
    {
        public ProductValidator()
        {
            var message = "Two of fields 'VAT', 'PriceWithVAT' and 'Price' are required";
            RuleFor(p => p.VAT).NotNull().When(p => p.PriceWithVAT == null || p.Price == null)
                .WithMessage(message);
            RuleFor(p => p.Price).NotNull().When(p => p.PriceWithVAT == null || p.VAT == null)
                .WithMessage(message);
            RuleFor(p => p.PriceWithVAT).NotNull().When(p => p.Price == null || p.VAT == null)
                .WithMessage(message);
        }
    }
}